import React from "react"

type Props = {
  label: string
  name: string
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  value: string
  symbol?: string
  disabled?: boolean
}

export function TextInput({
  label,
  name,
  onChange,
  value,
  symbol,
  disabled,
}: Props) {
  return (
    <label className="flex flex-col w-full">
      <span className="font-bold text-lg">{label}</span>
      <div className="relative">
        <input
          type="text"
          className="w-full border-black border-2 disabled:bg-gray-400 disabled:cursor-not-allowed rounded-lg p-4 text-lg"
          name={name}
          value={value}
          onChange={onChange}
          disabled={disabled}
        />
        {symbol ? (
          <div className="flex items-center justify-center w-32 text-center bg-black text-white text-lg h-full px-4 absolute right-0 top-0 rounded-tr-lg rounded-br-lg">
            {symbol}
          </div>
        ) : null}
      </div>
    </label>
  )
}
