// All of this is lifted from sushiswap/sushiswap-interface. No need to reinvent
// the wheel.
import { useMemo } from "react"
import { Contract } from "@ethersproject/contracts"
import { JsonRpcSigner, Web3Provider } from "@ethersproject/providers"
import { AddressZero } from "@ethersproject/constants"
import { getAddress } from "@ethersproject/address"

import ERC20_ABI from "../constants/abis/ERC20.json"
import SUSHI_ROLL_ABI from "../constants/abis/SushiRoll.json"
import UNISWAP_V2_PAIR_ABI from "../constants/abis/UniswapV2Pair.json"
import { useActiveWeb3React } from "./useWeb3React"

// Both Rinkeby
const SUSHI_ROLL_ADDRESS = "0xCaAbdD9Cf4b61813D4a52f980d6BC1B713FE66F5"
const UNISWAP_V2_PAIR_ADDRESS = "0x8B22F85d0c844Cf793690F6D9DFE9F11Ddb35449"

export function isAddress(value: any): string | false {
  try {
    return getAddress(value)
  } catch {
    return false
  }
}

// account is not optional
export function getSigner(
  library: Web3Provider,
  account: string
): JsonRpcSigner {
  return library.getSigner(account).connectUnchecked()
}

// account is optional
export function getProviderOrSigner(
  library: Web3Provider,
  account?: string
): Web3Provider | JsonRpcSigner {
  return account ? getSigner(library, account) : library
}

// account is optional
export function getContract(
  address: string,
  ABI: any,
  library: Web3Provider,
  account?: string
): Contract {
  if (!isAddress(address) || address === AddressZero) {
    throw Error(`Invalid 'address' parameter '${address}'.`)
  }

  return new Contract(
    address,
    ABI,
    getProviderOrSigner(library, account) as any
  )
}

export function useContract(
  address: string | undefined,
  ABI: any,
  withSignerIfPossible = true
): Contract | null {
  const { library, account } = useActiveWeb3React()

  return useMemo(() => {
    if (!address || !ABI || !library) return null
    try {
      return getContract(
        address,
        ABI,
        library,
        withSignerIfPossible && account ? account : undefined
      )
    } catch (error) {
      console.error("Failed to get contract", error)
      return null
    }
  }, [address, ABI, library, withSignerIfPossible, account])
}

export function useSushiRollContract(
  withSignerIfPossible?: boolean
): Contract | null {
  // Contract is the same regardless of testnet network, so should be fine to
  // just use the address and not need to do a chainId lookup like
  // sushiswap-interface does
  return useContract(SUSHI_ROLL_ADDRESS, SUSHI_ROLL_ABI, withSignerIfPossible)
}

export function useUniswapV2PairContract(
  withSignerIfPossible?: boolean
): Contract | null {
  return useContract(
    UNISWAP_V2_PAIR_ADDRESS,
    UNISWAP_V2_PAIR_ABI,
    withSignerIfPossible
  )
}

export function useTokenContract(
  tokenAddress: string,
  withSignerIfPossible?: boolean
): Contract | null {
  return useContract(tokenAddress, ERC20_ABI, withSignerIfPossible)
}
