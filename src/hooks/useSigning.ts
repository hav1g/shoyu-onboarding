import { signERC2612Permit } from "eth-permit"
import { splitSignature } from "@ethersproject/bytes"
import { BigNumber } from "@ethersproject/bignumber"
import { getMessage, TypedData } from "eip-712"

import { useActiveWeb3React } from "./useWeb3React"
import { UNI_V2_RINKEBY_ADDRESS } from "../utils/constants"

type Signing = {
  owner: string
  spender: string
  value: BigNumber
}

export function useSigning({ owner, spender, value }: Signing) {
  const { account, chainId, library } = useActiveWeb3React()
  const signer = library?.getSigner(account as string)

  // THIS DON'T WORK LOL
  async function signMessage(nonce: BigNumber, deadline: number) {
    const signingMessage: TypedData = {
      types: {
        EIP712Domain: [
          { name: "name", type: "string" },
          { name: "version", type: "string" },
          { name: "chainId", type: "uint256" },
          { name: "verifyingContract", type: "address" },
        ],
        Permit: [
          { name: "owner", type: "address" },
          { name: "spender", type: "address" },
          { name: "value", type: "uint256" },
          { name: "nonce", type: "uint256" },
          { name: "deadline", type: "uint256" },
        ],
      },
      primaryType: "Permit",
      domain: {
        name: "Uniswap V2",
        // name: "Uniswap V2",
        version: "1",
        chainId,
        // This is for Rinkeby
        // verifyingContract: "0x7e8d0E1AD361ebA94ABC06898f52D9e2C4cda04b", // SLP
        // verifyingContract: "0xCaAbdD9Cf4b61813D4a52f980d6BC1B713FE66F5", // SushiRoll
        // verifyingContract: "0x9DD48787B9A9d8eFf36C7F7dB13C67b268F7e256", // UniswapV2Pair
        verifyingContract: "0x8b22f85d0c844cf793690f6d9dfe9f11ddb35449", // Uniswap V2
      },
      message: {
        owner,
        spender,
        value: value.toString(),
        nonce: nonce.toNumber(),
        deadline,
      },
    }
    const digest = getMessage(signingMessage, true)

    const signature = await signer?.signMessage(digest)

    const { v, r, s } = splitSignature(signature as string)

    return { v, r, s }
  }

  // THIS DOES THO LOL
  async function signPermit(deadline: number, nonce: BigNumber) {
    const { v, r, s } = await signERC2612Permit(
      library,
      UNI_V2_RINKEBY_ADDRESS,
      owner,
      spender,
      value.toString(),
      deadline,
      nonce.toNumber()
    )

    return { v, r, s }
  }

  return {
    signMessage,
    signPermit,
  }
}
