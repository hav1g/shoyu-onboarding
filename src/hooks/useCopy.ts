import { useCallback, useState } from "react"

export function useCopy(ref: React.RefObject<HTMLDivElement>) {
  const [copied, setCopied] = useState<boolean>(false)

  const copy = useCallback(async () => {
    if (navigator == null || !ref?.current?.innerText) {
      // Bail if we don't have what we need to copy
      setCopied(false)
      return null
    }

    try {
      await navigator.clipboard.writeText(ref?.current?.innerText)

      setCopied(true)
    } catch (e) {
      console.error("Something went wrong trying to copy to clipboard")
      setCopied(false)
    }
  }, [ref])

  return {
    copy,
    copied,
  }
}
