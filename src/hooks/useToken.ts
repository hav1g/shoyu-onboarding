import { BigNumber } from "@ethersproject/bignumber"
import { useState } from "react"
import { useTokenContract } from "./useContract"

export type UseToken = {
  balance: BigNumber | null
  balanceOf: (address: string) => Promise<any>
  totalSupply: () => Promise<any>
}

export function useToken(tokenAddress: string): UseToken {
  const [balance, setBalance] = useState<BigNumber | null>(null)

  const token = useTokenContract(tokenAddress)

  async function balanceOf(address: string) {
    try {
      const n = await token?.balanceOf(address)

      setBalance(n)

      return n
    } catch (err) {
      console.error("Error getting balance of token", err)
    }
  }

  async function totalSupply() {
    try {
      const supply = await token?.totalSupply()

      return supply
    } catch (err) {
      console.error("Error getting total supply", err)
    }
  }

  return {
    balance,
    balanceOf,
    totalSupply,
  }
}
